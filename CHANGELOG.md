## [0.2.0] - 2019.08.02
- Repaint with pixelRatio
- Extract SnapshotRepaint
- Add Thresholdable for StylusPainter

## [0.1.1] - 2019.07.26
- Add StylusPointer
- Remove notepad_kit from dev_dependency

## [0.1.0] - 2019.07.24

- Add LayerView
- Add CircleIndicatorPainter
- Add LineStrokePainter
- Add PathStrokePainter
- Add PeriodicRepainter
