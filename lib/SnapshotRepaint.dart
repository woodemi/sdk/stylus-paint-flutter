import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class SnapshotRepaint extends StatefulWidget {
  final _ImagePainter _imagePainter;
  final Widget child;

  Stream<int> snapshotRequestStream;
  StreamController<int> snapshotResponseController;

  SnapshotRepaint({
    paintSize,
    this.snapshotRequestStream,
    this.snapshotResponseController,
    this.child
  }): this._imagePainter = _ImagePainter(paintSize);

  @override
  State<StatefulWidget> createState() => _SnapshotRepaintState();
}

class _SnapshotRepaintState extends State<SnapshotRepaint> {
  StreamSubscription<int> _subscription;

  @override
  void initState() {
    super.initState();
    _subscription = widget.snapshotRequestStream.listen((index) {
      paintBoundary(index);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _subscription?.cancel();
    widget.snapshotResponseController?.close();
  }

  final _boundaryKey = GlobalKey();

  void paintBoundary(int index) async {
    RenderRepaintBoundary renderRepaintBoundary = _boundaryKey.currentContext?.findRenderObject();
    var image = await renderRepaintBoundary?.toImage(pixelRatio: ui.window.devicePixelRatio);
    widget._imagePainter.setImage(image);
    widget.snapshotResponseController.add(index);
  }

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: _boundaryKey,
      child: CustomPaint(
        child: widget.child,
        painter: widget._imagePainter,
      ),
    );
  }
}

class _ImagePainter extends CustomPainter with ChangeNotifier {
  final Size paintSize;

  ui.Image _image;

  final _paint = Paint();

  _ImagePainter(this.paintSize);

  @override
  void paint(Canvas canvas, Size size) {
    if (_image != null) {
      var srcRect = Offset.zero & Size(_image.width.toDouble(), _image.height.toDouble());
      canvas.drawImageRect(_image, srcRect, Offset.zero & paintSize, _paint);
    }
  }

  void setImage(ui.Image image) {
    _image = image;
    notifyListeners();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}