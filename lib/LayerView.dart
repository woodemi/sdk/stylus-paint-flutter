import 'dart:async';

import 'package:flutter/widgets.dart';

import 'StylusPainter.dart';
import 'StylusPointer.dart';

class LayerView extends StatefulWidget {
  final StylusPainter painter;

  final Size paintSize;

  final Stream<StylusPointer> syncPointerStream;

  LayerView(
    this.painter,
    this.paintSize,
    this.syncPointerStream,
  ) : assert(painter != null),
      assert(paintSize != null);

  @override
  State<StatefulWidget> createState() => _LayerViewState();
}

class _LayerViewState extends State<LayerView> {
  StreamSubscription<StylusPointer> _syncPointerSubscription;

  @override
  void initState() {
    super.initState();
    _syncPointerSubscription = widget.syncPointerStream.listen((pointer) {
      print('listen ${pointer.x}, ${pointer.y}, ${pointer.p}');
      widget.painter?.append(pointer);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _syncPointerSubscription?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: CustomPaint(
        size: widget.paintSize,
        painter: widget.painter,
      ),
    );
  }
}