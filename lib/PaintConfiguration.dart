import 'dart:math';
import 'dart:ui';

import 'StylusPointer.dart';

const double k = 1;
const double a3 = 1.2;
const double b3 = 0.002;

// TODO Init programmatically
PaintConfiguration sPaintConfiguration = PaintConfiguration(1024);

class PaintConfiguration {
  int maxPressure;

  PaintConfiguration(this.maxPressure);

  Paint getPaint(StylusPointer pointer) {
    return Paint()
      ..strokeWidth = _getWidth(pointer);
  }

  double _getWidth(StylusPointer pointer) {
    // TODO Normalized with maxPressure
    return k * pow(a3, b3 * pointer.p);
  }
}