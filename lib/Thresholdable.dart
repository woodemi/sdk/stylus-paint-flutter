import 'dart:async';

abstract class Thresholdable {
  int get threshold;
  set threshold(int value);

  Stream<int> get thresholdNotifyStream;
}