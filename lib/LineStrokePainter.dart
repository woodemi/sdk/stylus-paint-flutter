import 'dart:async';
import 'dart:ui';

import 'package:flutter/foundation.dart';

import 'StylusPainter.dart';
import 'StylusPointer.dart';
import 'Thresholdable.dart';

class LineStroke extends Iterable<StylusPointer> {

  final _pointers = List<StylusPointer>();

  @override
  Iterator<StylusPointer> get iterator => _pointers.iterator;

  void append(StylusPointer pointer) {
    _pointers.add(pointer);
  }
}

class LineStrokePainter extends StylusPainter with ChangeNotifier implements Thresholdable {
  static final TAG = 'LineStrokePainter';

  LineStrokePainter(
    scaleRatio, {
    stylusPaint
  }): super(
    scaleRatio,
    stylusPaint
  );

  final _strokes = List<LineStroke>();

  LineStroke _s;

  int _pointCount = 0;

  int _thresholdCount = 0;

  @override
  int threshold = 5000;

  // FIXME Close controller
  final _thresholdNotifyController = StreamController<int>();

  @override
  Stream<int> get thresholdNotifyStream => _thresholdNotifyController.stream;

  @override
  void append(StylusPointer pointer) {
    if (pointer.p == 0) {
      _s = null; // Ensure no stroke
      return;
    }

    if (_pointCount == threshold - 1) {
      _pointCount = 0;
      _strokes.clear();
      _s = null;
      _thresholdNotifyController.add(_thresholdCount++);
      notifyListeners();
      return;
    }

    if (_s == null) {
      _s = LineStroke(); // Stroke begins with pointer down
      _strokes.add(_s);
    }
    _pointCount++;
    _s.append(pointer);
    notifyListeners();
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (_strokes.length == 0) return;

    for (var s in _strokes) {
      _drawStroke(s, canvas, stylusPaint);
    }
  }

  void _drawStroke(LineStroke s, Canvas canvas, Paint paint) {
    StylusPointer pre;
    for (var cur in s) {
      if (pre != null) {
        var preOffset = Offset(pre.x * scaleRatio, pre.y * scaleRatio);
        var curOffset = Offset(cur.x * scaleRatio, cur.y * scaleRatio);
        canvas.drawLine(preOffset, curOffset, paint);
      }
      pre = cur;
    }
  }
}
