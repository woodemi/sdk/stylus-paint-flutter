import 'dart:ui';

import 'package:flutter/foundation.dart';

import 'StylusPainter.dart';
import 'StylusPointer.dart';

class CircleIndicatorPainter extends StylusPainter with ChangeNotifier {
  static final _TAG = 'CircleIndicatorPainter';

  CircleIndicatorPainter(
    scaleRatio, {
      stylusPaint,
      this.paintRadius = 30,
  }): super(
    scaleRatio,
    stylusPaint,
  );

  final double paintRadius;

  StylusPointer pointer;

  @override
  void append(StylusPointer pointer) {
    this.pointer = pointer;
    notifyListeners();
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (pointer != null) {
      var offset = Offset(pointer.x * scaleRatio, pointer.y * scaleRatio);
      canvas.drawCircle(offset, paintRadius, stylusPaint);
    }
  }
}