import 'dart:async';
import 'dart:collection';
import 'dart:ui';

import 'package:flutter/foundation.dart';

import 'PaintConfiguration.dart';
import 'StylusPainter.dart';
import 'StylusPointer.dart';
import 'Thresholdable.dart';

const CUBIC_NUM = 4;

class PathStroke {
  static final _TAG = 'PathStroke';

  final double scaleRatio;
  
  final _cubicQueue = ListQueue<StylusPointer>(CUBIC_NUM);
  final _paths = List<Path>();
  final _paints = List<Paint>();

  PathStroke(this.scaleRatio);

  void append(StylusPointer pointer) {
    print('$_TAG append');
    _cubicQueue.addLast(pointer);
    if (_cubicQueue.length < CUBIC_NUM) return;

    var pre = _cubicQueue.elementAt(0);
    var c1 = _cubicQueue.elementAt(1);
    var c2 = _cubicQueue.elementAt(2);
    var cur = _cubicQueue.elementAt(3);
    _paths.add(Path()
      ..moveTo(pre.x * scaleRatio, pre.y * scaleRatio)
      ..cubicTo(c1.x * scaleRatio, c1.y * scaleRatio, c2.x * scaleRatio, c2.y * scaleRatio, cur.x * scaleRatio, cur.y * scaleRatio));

    _paints.add(sPaintConfiguration.getPaint(pointer));

    _cubicQueue.removeFirst();
  }
}

class PathStrokePainter extends StylusPainter with ChangeNotifier implements Thresholdable {
  static final TAG = 'PathStrokePainter';

  final _strokes = List<PathStroke>();

  PathStroke _s;

  PathStrokePainter(
    scaleRatio, {
    stylusPaint
  }): super(
    scaleRatio,
    stylusPaint
  );

  int _pointCount = 0;

  int _thresholdCount = 0;

  @override
  int threshold = 2000;

  // FIXME Close controller
  final _thresholdNotifyController = StreamController<int>();

  @override
  Stream<int> get thresholdNotifyStream => _thresholdNotifyController.stream;

  @override
  void append(StylusPointer pointer) {
    if (pointer.p == 0) {
      _s = null; // Ensure no stroke
      return;
    }

    if (_pointCount == threshold - 1) {
      _pointCount = 0;
      _strokes.clear();
      _s = null;
      _thresholdNotifyController.add(_thresholdCount++);
      notifyListeners();
      return;
    }

    if (_s == null) {
      _s = PathStroke(scaleRatio); // Stroke begins with pointer down
      _strokes.add(_s);
    }
    _pointCount++;
    _s.append(pointer);
    notifyListeners();
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (_strokes.length == 0) return;

    for (var stroke in _strokes) {
      for (var i = 0; i < stroke._paints.length; i++) {
        canvas.drawPath(stroke._paths[i], stroke._paints[i]);
      }
    }
  }
}