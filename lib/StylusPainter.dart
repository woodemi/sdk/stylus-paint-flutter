import 'package:flutter/material.dart';
import 'StylusPointer.dart';

abstract class StylusPainter extends CustomPainter {
  static final _defaultPaint =  Paint()..color = Colors.red;

  StylusPainter(
    this.scaleRatio,
    stylusPaint
  ): this.stylusPaint = stylusPaint ?? StylusPainter._defaultPaint;

  final double scaleRatio;

  final Paint stylusPaint;

  void append(StylusPointer pointer);

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }

  @override
  bool hitTest(Offset position) => null;

  @override
  get semanticsBuilder => null;

  @override
  bool shouldRebuildSemantics(CustomPainter oldDelegate) => shouldRepaint(oldDelegate);
}
